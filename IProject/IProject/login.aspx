﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="IProject.WebForm5" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>

    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <%--Bootstrap Core CSS--%> 
    
    <style>
        body{
            background-color: aliceblue;
        }


    </style>

</head>
<body>
    
    <form id="frmLogin" runat="server">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4" style="padding-top: 100px;">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Please Sign In</h3>
                        </div>
                        <div class="panel-body">
                            <fieldset>
                                <div class="form-group">
                                    <asp:TextBox class="form-control" ID="uname" runat="server" placeholder="E-mail" type="text" autofocus="autofocus"></asp:TextBox>                           
                                </div>
                                <div class="form-group">
                                    <asp:TextBox class="form-control" ID="password" runat="server" placeholder="Password" type="password"></asp:TextBox>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <asp:Button ID="LecturerLogin" class="btn btn-lg btn-success btn-block" runat="server" Text="Lecturer Login" OnClick="LecturerLogin_Click" />
                                    </div>
                                    <div class="col-md-6">
                                        <asp:Button ID="StudentLogin" class="btn btn-lg btn-primary btn-block" runat="server" Text="Student Login" OnClick="StudentLogin_Click" />
                                    </div>
                                </div>
                                <br />
                                <asp:Label ID="LoginErrorTxt" CssClass="text-danger" runat="server" Text=""></asp:Label>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>





</body>
</html>
