﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="schedule.aspx.cs" Inherits="IProject.schedule" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-primary">
                <div class="panel-heading"><b>Lecture Schedule</b></div>
                <div class="panel-body">
                    <br />
                    <asp:GridView ID="GridView1" runat="server" CssClass="table table-condensed" DataSourceID="timetable">
                    </asp:GridView>
                </div>
            </div>

            <div class="panel panel-success">
                <div class="panel-heading"><b>Lab Schedule</b></div>
                <div class="panel-body">
                    <br />
                    <asp:GridView ID="GridView2" runat="server" CssClass="table table-condensed" DataSourceID="LabSession"></asp:GridView>
                    <asp:ObjectDataSource ID="LabSession" runat="server" SelectMethod="Lab_Session_Time" TypeName="IProject.ServiceReference1.Service1Client">
                        <SelectParameters>
                            <asp:SessionParameter DefaultValue="null" Name="B_code" SessionField="bCode" Type="String" />
                            <asp:SessionParameter DefaultValue="null" Name="lecName" SessionField="lecName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </div>
            </div>

            <div class="panel panel-warning">
                <div class="panel-heading"><b>Sport Schedule</b></div>
                <div class="panel-body">
                    <br />
                    <asp:GridView ID="GridView3" runat="server" CssClass="table table-condensed" DataSourceID="SportTimeTable"></asp:GridView>
                    <asp:ObjectDataSource ID="SportTimeTable" runat="server" SelectMethod="SportTimeTable" TypeName="IProject.ServiceReference1.Service1Client">
                        <SelectParameters>
                            <asp:SessionParameter Name="lecName" SessionField="lecName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </div>
            </div>
            
        </div>
    </div>

















    <asp:ObjectDataSource ID="timetable" runat="server" SelectMethod="TimeTableInfo" TypeName="IProject.ServiceReference1.Service1Client">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="Student" Name="table" SessionField="type" Type="String" />
            <asp:SessionParameter DefaultValue="null" Name="B_code" SessionField="bCode" Type="String" />
            <asp:SessionParameter DefaultValue="null" Name="lecEmail" SessionField="userName" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>










</asp:Content>
