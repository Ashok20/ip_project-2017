﻿using IProject.ServiceReference1;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IProject
{
    public partial class WebForm5 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void LecturerLogin_Click(object sender, EventArgs e)
        {
            if (uname.Text != "" && password.Text != "")
            {
                Session["userName"] = null;
                Session["pass"] = null;
                Session["type"] = null;
                Session["bCode"] = null;
                Session["lecName"] = null;

                LoginErrorTxt.Text = "Invalid Email or Password";
                Service1Client obj = new Service1Client();
                DataTable dt = obj.ViewUserInfo("Lecturer", uname.Text, password.Text);

                if (dt.Rows.Count > 0 && dt.Rows.Count < 2)
                {

                    Session["userName"] = uname.Text;
                    Session["pass"] = password.Text;
                    Session["type"] = "Lecturer";
                    Session["lecName"] = dt.Rows[0].Field<string>("L_Name");
                    Response.Redirect("schedule.aspx");

                }
                else
                {
                    LoginErrorTxt.Text = "Invalid Email or Password";
                    uname.Text = "";
                    password.Text = "";
                    uname.Focus();
                }
            }
            else
            {
                uname.Focus();
                LoginErrorTxt.Text = "Enter Email or Password";
            }

        }

        protected void StudentLogin_Click(object sender, EventArgs e)
        {
            if (uname.Text != "" && password.Text != "")
            {
                Session["userName"] = null;
                Session["pass"] = null;
                Session["type"] = null;
                Session["bCode"] = null;
                Session["lecName"] = null;

                LoginErrorTxt.Text = "Invalid Email or Password";
                Service1Client obj = new Service1Client();
                DataTable dt = obj.ViewUserInfo("Student", uname.Text, password.Text);

                if (dt.Rows.Count > 0 && dt.Rows.Count < 2)
                {

                    Session["userName"] = uname.Text;
                    Session["pass"] = password.Text;
                    Session["type"] = "Student";
                    Session["bCode"] = dt.Rows[0].Field<string>("B_Code");

                    Response.Redirect("schedule.aspx");

                }
                else
                {
                    LoginErrorTxt.Text = "Invalid Email or Password";
                    uname.Text = "";
                    password.Text = "";
                    uname.Focus();
                }
            }
            else
            {
                uname.Focus();
                LoginErrorTxt.Text = "Enter Email or Password";
            }
        }
    }
}