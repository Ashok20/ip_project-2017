﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="IProject.WebForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>HomePage</title>
    <link href="Content/aniHome.css" rel="stylesheet" />
    <link href="Content/faculty.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

   

    <header class="business-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-12  text-center">
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <hr />
                    <h1 class="tagline">Find Greatness in Leading, Find Greatness in Technology, Find Greatness in Creativity</h1>
                    <hr />
                    <p>Advancing knowledge, transforming lives.</p>
                    <p>
                        The NSBM is renowned for high quality, internationally-leading education, research and innovation. We make a positive difference to people’s lives. With a truly global outlook, we are an inclusive and inspiring university community. Our students are our partners on their journey of discovery.
                    </p>
                    <br>
                </div>
            </div>
        </div>
    </header>


    <div class="container">
        <div class="page-header">
            <h1 id="timeline">
                <center>Timeline</center>
            </h1>
        </div>
        <ul class="timeline">
            <li>
                <div class="timeline-badge"><i class="glyphicon glyphicon-check"></i></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4 class="timeline-title">School of Computing</h4>
                        <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i>11 hours ago via Twitter</small></p>
                    </div>
                    <div class="timeline-body">
                        <p>The scool of Computing is an another main school at NSBM which provides world class training and education in Computing, Information Technology, Design, Mathematics and Statistics at undergraduate as well as post graduate levels. The School of Computing too partners up with the world top ranking universities, University College Dublin, Ireland, Limkokwing University of Creative Arts & Technology, Malaysia and University of Plymouth, UK to provide undergraduates with highly recognized International Degrees. The innovative teaching methods along with the latest state of the art equipment form the perfect blend that motivates our students to do their best and reach their goals with ease. </p>
                        <p>The school provides top notch researching, training and development  services that will help students acquire new knowledge along with the best practices in their respective disciplines. The School of Computing aims to be among the foremost centre of excellence in Research and Development (R&D) and advance education in Computing while taking into consideration National as well as Regional requirements for Information and Communication Technology. Further, The School of Computing places equal emphasis on both theory and practice of all aspects of the Computing field. Therefore, our students will have sufficient hands on experience to take up any working assignment in their respective IT fields at the end of their degree programmes.</p>
                    </div>
                    <br />
                    <div class="timeline-heading">
                         <h4 class="timeline-title">IT Division</h4>
                    </div>
                    <div class="timeline-body">
                        <p>As forward thinking school in higher education NSBM assures the vital need to make learning relevant to industry expectations. NSBM has forged strategic collaborations with leading universities in the world that enable its students to expose themselves with global thinking and best in global industry.</p>
                    </div>
                </div>
            </li>
            <li class="timeline-inverted">
                <div class="timeline-badge warning"><i class="glyphicon glyphicon-credit-card"></i></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4 class="timeline-title">Scool of Business</h4>
                        <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i>11 hours ago via Twitter</small></p>
                    </div>
                    <div class="timeline-body">
                        <p>The School of Business of NSBM is the ideal institute for any undergraduate interested in pursuing a career in the Business field. The School nurtures students with a business mind and moulds them into fully fledged business leaders of the future. The drive behind achieving this goal is the passion for excellence and perfection that surrounds the School of Business in its methods of teaching, learning, research and networking with the business community.</p>
                        <p>The School of Business offers many degree programs: Business Management, Human Resource Management, Law and Accounting in collaboration with world renowned universities, University College Dublin, Ireland, Limkokwing University Malaysia and University of Plymouth, UK. This gives our graduates the opportunity to gain an International degree while living and working in Sri Lanka.</p>
                        <p>Students will find learning at NSBM’s School of Business quite a unique and interesting experience as undergraduates are also given the taste of real life business experiences while learning the theories behind it at class. Through this process, the school strives to prepare business undergraduates to face any challenges in the real business world as they will be equipped with excellent problem solving and analytical capabilities. The School of Business takes special care to ensure that all students are provided with intellectual depth and abundant resources as well as individual attention.</p>
                        <p>Each department of the School of Business is dedicated to its students and in providing them the best possible educational experience. So, if you’re looking for a bright future in Business, NSBM School of Business is the place to be!</p>
                    </div>
                     <br />
                    <div class="timeline-heading">
                         <h4 class="timeline-title">Career Development</h4>
                    </div>
                    <div class="timeline-body">
                        <p>Career Development Programmes are an inclusive part of the institutional development. The students are motivated for upgrading their skills and personality on a customary basis. Particularly, the students are treated and trained for analytical skills, human skills, leadership qualities and orientations, communication skills</p>
                    </div>
                </div>
            </li>
            <li>
                <div class="timeline-badge danger"><i class="glyphicon glyphicon-credit-card"></i></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4 class="timeline-title">School of Engineering</h4>
                         <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i>11 hours ago via Twitter</small></p>
                    </div>
                    <div class="timeline-body">
                        <p>The School of Engineering is the newest addition to the faculties at NSBM. The School of Engineering offers degrees in Computer Engineering and Electronics in collaboration with the internationally acclaimed universities. </p>
                        <p>NSBM’s School of Engineering prides itself for keeping up to date with the advancing technologies through research and development activities, staff training and networking with international communities. Further, the school hopes to acquire, promote, develop and disseminate knowledge of engineering sciences in order to improve the quality of life by equipping future generations with the right attitudes and skills to grow into competent individuals who could be considered an asset to the nation.</p>
                    </div>
                    <br />
                    <div class="timeline-heading">
                         <h4 class="timeline-title">Aims of the School</h4>
                    </div>
                    <div class="timeline-body">
                        <p>The primary aim of the School is forming engineers of the highest quality who, with experience, should be able to hold responsible positions at the highest levels of the profession, possessing the wisdom to recognize their professional development.</p>
                    </div>
                </div>
            </li>
    </div>
    </li>
    </ul>
</div>







</asp:Content>
