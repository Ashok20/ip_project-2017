﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace NSBMService
{
    public class Instructors
    {
        public string Ins_Id;
        public string I_Name;
        public string Contact;
        public string Email;
        public string Faculty;

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["NSBMdb"].ToString());

        public string AddInstructor()
        {
            SqlCommand cmd = new SqlCommand("insert into Instructor values (@Ins_Id,@I_Name,@Contact,@Email,@Faculty)", con);
            cmd.Parameters.AddWithValue("@Ins_Id", Ins_Id);
            cmd.Parameters.AddWithValue("@I_Name", I_Name);
            cmd.Parameters.AddWithValue("@Contact", Contact);
            cmd.Parameters.AddWithValue("@Email", Email);
            cmd.Parameters.AddWithValue("@Faculty", Faculty);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return "Record added";
            }
            catch (SqlException ex)
            {
                return ex.ToString();
            }
        }

        public string DelInstructor(string Ins_Id)
        {
            SqlCommand cmd = new SqlCommand("delete from dbo.Instructor where Ins_id =" + Ins_Id , con);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return "Record deleted";
            }
            catch (SqlException ex)
            {
                return ex.ToString();
            }
        }

        public string UpdateInstructors(string Ins_Id)
        {
            SqlCommand cmd = new SqlCommand("update Instructor set Ins_id = @Ins_Id,I_Name = @I_Name,Contact = @Contact,Email = @Email,Faculty = @Faculty where Ins_id =" + Ins_Id, con);
            cmd.Parameters.AddWithValue("@Ins_Id", Ins_Id);
            cmd.Parameters.AddWithValue("@I_Name", I_Name);
            cmd.Parameters.AddWithValue("@Contact", Contact);
            cmd.Parameters.AddWithValue("@Email", Email);
            cmd.Parameters.AddWithValue("@Faculty", Faculty);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return "Record updated";
            }
            catch (SqlException ex)
            {
                return ex.ToString();
            }
        }



        public DataTable ViewInstructors()
        {
            SqlCommand cmd = new SqlCommand("select * from Instructor", con);
            DataTable dt = new DataTable();

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            adpt.Fill(dt);
            dt.TableName = "Instructor";
            return dt;
        }

    }
}