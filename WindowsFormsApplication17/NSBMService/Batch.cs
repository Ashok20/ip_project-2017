﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace NSBMService
{
    public class Batch
    {
        public string B_code;
        public string Capacity;
        public string B_no;
        public string S_date;
        public string E_date;

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["NSBMdb"].ToString());

        public string AddBatch()
        {
            SqlCommand cmd = new SqlCommand("insert into Batch values(@B_code,@Capacity,@B_no,@S_date,@E_date)",con);
            cmd.Parameters.AddWithValue("@B_code", B_code);
            cmd.Parameters.AddWithValue("@Capacity", Capacity);
            cmd.Parameters.AddWithValue("@B_no", B_no);
            cmd.Parameters.AddWithValue("@S_date", S_date);
            cmd.Parameters.AddWithValue("@E_date", E_date);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return "Record added";
            }
            catch (SqlException ex)
            {
                return ex.ToString();
            }
        }

        public string DelBatch(string B_code)
        {
            SqlCommand cmd = new SqlCommand("delete from Batch where B_code = " + B_code, con);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return "Record deleted";
            }
            catch (SqlException ex)
            {
                return ex.ToString();
            }
        }


        public DataTable ViewBatch()
        {
            SqlCommand cmd = new SqlCommand("select * from Batch", con);
            DataTable dt = new DataTable();

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            adpt.Fill(dt);
            dt.TableName = "Batch";
            return dt;
        }

        public string UpdateBatch(string B_code)
        {
            SqlCommand cmd = new SqlCommand("update Batch set B_code = @B_code,Capacity = @Capacity,B_no=@B_no,S_date=@S_date,E_date=@E_date where B_code =" + B_code, con);
            cmd.Parameters.AddWithValue("@B_code", B_code);
            cmd.Parameters.AddWithValue("@Capacity", Capacity);
            cmd.Parameters.AddWithValue("@B_no", B_no);
            cmd.Parameters.AddWithValue("@S_date", S_date);
            cmd.Parameters.AddWithValue("@E_date", E_date);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return "Record updated";
            }
            catch (SqlException ex)
            {
                return ex.ToString();
            }

        }




        }
}