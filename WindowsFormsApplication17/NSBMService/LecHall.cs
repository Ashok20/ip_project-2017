﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace NSBMService
{
    public class LecHall
    {

        public string lec_id;
        public string date;
        public string S_time;
        public string E_time;
        public string B_code;
        public string Lecturer;
        public string H_id;
        public string M_name;

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["NSBMdb"].ToString());

        public string AddLec()
        {
            SqlCommand cmd = new SqlCommand("insert into Lecture values(@lec_id,@B_code,@date,@S_time,@E_time,@M_name,@Lecturer,@H_id)", con); 

            cmd.Parameters.AddWithValue("@lec_id", lec_id);
            cmd.Parameters.AddWithValue("@B_code", B_code);
            cmd.Parameters.AddWithValue("@date", date);
            cmd.Parameters.AddWithValue("@S_time", S_time);
            cmd.Parameters.AddWithValue("@E_time", E_time);
            cmd.Parameters.AddWithValue("@M_name", M_name);
            cmd.Parameters.AddWithValue("@Lecturer", Lecturer);
            cmd.Parameters.AddWithValue("@H_id", H_id);
            
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return "Record added";
            }
            catch (SqlException ex)
            {
                return ex.ToString();
            }
        }

        public string DelLec(string lec_id)
        {
            
            SqlCommand cmd = new SqlCommand("delete from Lecture where Lecture_id =" + lec_id, con);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return "Record deleted";
            }catch(SqlException ex)
            {
                return ex.ToString();
            }
        }

        public string UpdateLec(string lec_id) 
        {
            SqlCommand cmd = new SqlCommand("update Lecture set Lecture_id = @lec_id,B_code = @B_code,Lecture_Date = @date,S_time = @S_time,E_time = @E_time,M_name = @M_name,Lecturer = @Lecturer,H_id = @H_id where Lecture_id =" + lec_id,con);

            cmd.Parameters.AddWithValue("@lec_id", lec_id);
            cmd.Parameters.AddWithValue("@B_code", B_code);
            cmd.Parameters.AddWithValue("@date", date);
            cmd.Parameters.AddWithValue("@S_time", S_time);
            cmd.Parameters.AddWithValue("@E_time", E_time);
            cmd.Parameters.AddWithValue("@M_name", M_name);
            cmd.Parameters.AddWithValue("@Lecturer", Lecturer);
            cmd.Parameters.AddWithValue("@H_id", H_id);
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return "Record updated";
            }
            catch (SqlException ex)
            {
                return ex.ToString();
            }
        }


        public DataTable ViewLec()
        {
            SqlCommand cmd = new SqlCommand("select * from Lecture", con);
            DataTable dt = new DataTable();

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            adpt.Fill(dt);
            dt.TableName = "Lecture";
            return dt;
        }


    }
}