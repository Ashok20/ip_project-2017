﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;


namespace NSBMService
{
    public class Sports
    {
        public string All_ID;
        public string Allocation;
        public string Club;
        public string Date;
        public string S_time;
        public string E_time;

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["NSBMdb"].ToString());

        public string AddSport()
        {
            SqlCommand cmd = new SqlCommand("insert into Sports_All values (@All_ID,@Allocation,@Clubs,@Date,@S_time,@E_time)", con);

            cmd.Parameters.AddWithValue("@All_ID",All_ID);
            cmd.Parameters.AddWithValue("@Allocation",Allocation);
            cmd.Parameters.AddWithValue("@Clubs",Club);
            cmd.Parameters.AddWithValue("@Date", Date);
            cmd.Parameters.AddWithValue("@S_time",S_time);
            cmd.Parameters.AddWithValue("@E_time", E_time);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return "Record added";
            }
            catch (SqlException ex)
            {
                return ex.ToString();
            }

        }


        public string DelSport(string All_ID)
        {
            SqlCommand cmd = new SqlCommand("delete from Sports_All where All_ID = " + All_ID, con);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return "Record deleted";
            }
            catch (SqlException ex)
            {
                return ex.ToString();
            }
        }

        public string UpdateSport(string All_ID){ 
            SqlCommand cmd = new SqlCommand("update Sports_All set All_ID = @All_ID, Allocation = @Allocation, Club = @Club, S_date = @Date, S_time= @S_time,E_time = @E_time where All_ID =" + All_ID, con);

            cmd.Parameters.AddWithValue("@All_ID", All_ID);
            cmd.Parameters.AddWithValue("@Allocation", Allocation);
            cmd.Parameters.AddWithValue("@Club", Club);
            cmd.Parameters.AddWithValue("@Date", Date);
            cmd.Parameters.AddWithValue("@S_time", S_time);
            cmd.Parameters.AddWithValue("@E_time", E_time);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return "Record updated";
            }
            catch (SqlException ex)
            {
                return ex.ToString();
            }

        }



        public DataTable ViewSport()
        {
            SqlCommand cmd = new SqlCommand("select * from Sports_All",con);
            DataTable dt = new DataTable();

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            adpt.Fill(dt);
            dt.TableName = "Sports_All";
            return dt;
        }

    }
}