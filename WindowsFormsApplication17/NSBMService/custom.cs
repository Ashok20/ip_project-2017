﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace NSBMService
{
    public class custom
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["NSBMdb"].ToString());

        public DataTable ViewUserInfo(string table, string uName, string pass)
        {
            SqlCommand cmd = null;
            if (table == "Student")
            {
                cmd = new SqlCommand("select * from Student where Username='" + uName + "' and Password ='" + pass + "'", con);
            }
            else
            {
                cmd = new SqlCommand("select * from Lecturer where Email='" + uName + "' and Contact ='" + pass + "'", con);
            }

            DataTable dt = new DataTable();

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            adpt.Fill(dt);
            dt.TableName = "userInfo";
            return dt;
        }

        public DataTable TimeTableInfo(string table, string B_code, string lecEmail)
        {
            SqlCommand cmd = null;
            DataTable dt = new DataTable();
            if (table == "Student")
            {
                cmd = new SqlCommand("select * from Lecture where B_code ='" + B_code + "'", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                adpt.Fill(dt);
                dt.TableName = "stdTimeTable";
            }
            else
            {
                cmd = new SqlCommand("select * from Lecture where Lecturer = (select L_Name from Lecturer where Email = '" + lecEmail + "')", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                adpt.Fill(dt);
                dt.TableName = "lecTimeTable";
            }
            return dt;
        }


        public DataTable Lab_Session_Time(string B_code, string lecName)
        {
            SqlCommand cmd = null;
            DataTable dt = new DataTable();
            if (lecName != "")
            {
                cmd = new SqlCommand("select * from Lab_Session", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                adpt.Fill(dt);
                dt.TableName = "lecLab";
            }
            else
            {
                cmd = new SqlCommand("select * from Lab_Session where B_code = '" + B_code + "'", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                adpt.Fill(dt);
                dt.TableName = "stdLab";
            }
            return dt;
        }


        public DataTable SportTimeTable(string lecName)
        {
            SqlCommand cmd = null;
            DataTable dt = new DataTable();
            if (lecName == null)
            {
                cmd = new SqlCommand("select * from Sports_All", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                adpt.Fill(dt);
                dt.TableName = "lecLab";
            }
            dt = null;
            return dt;
        }



    }
}