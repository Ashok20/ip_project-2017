﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data;

namespace NSBMService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {

        //Lecture Halls
        public string AddLec(string lec_id, string B_code, string date, string S_time, string E_time, string M_name, string Lecturer, string H_id)
        {
            LecHall LH = new LecHall();
            LH.lec_id = lec_id;
            LH.date = date;
            LH.S_time = S_time;
            LH.E_time = E_time;
            LH.B_code = B_code;
            LH.Lecturer = Lecturer;
            LH.H_id = H_id;
            LH.M_name = M_name;
            return LH.AddLec();
        }

        public string DelLec(string lec_id)
        {
            LecHall LH = new LecHall();
            LH.lec_id = lec_id;
            return LH.DelLec(LH.lec_id);
        }

        public string UpdateLec(string lec_id, string B_code, string date, string S_time, string E_time, string M_name, string Lecturer, string H_id)
        {
            LecHall LH = new LecHall();
            //LH.lec_id = lec_id;
            LH.lec_id = lec_id;
            LH.date = date;
            LH.S_time = S_time;
            LH.E_time = E_time;
            LH.B_code = B_code;
            LH.Lecturer = Lecturer;
            LH.H_id = H_id;
            LH.M_name = M_name;
            return LH.UpdateLec(LH.lec_id);
        }

        public DataTable ViewLec()
        {
            LecHall LH = new LecHall();
            return LH.ViewLec();
        }


        //AddLab

        public string AddLab(string Session_id, string B_code,string Group_no, string date, string S_time, string E_time, string M_name, string Instructor, string H_id) {
            Labs lb = new Labs();
            lb.Session_id = Session_id;
            lb.B_code = B_code;
            lb.Group_no = Group_no;
            lb.date = date;
            lb.S_time = S_time;
            lb.E_time = E_time;
            lb.M_name = M_name;
            lb.Instructor = Instructor;
            lb.H_id = H_id;
            return lb.AddLab();
        }


        public string DelLab(string Session_id)
        {
            Labs lb = new Labs();
            lb.Session_id = Session_id;
            return lb.DelLab(lb.Session_id);
        }


        public string UpdateLab(string Session_id, string B_code, string Group_no, string date, string S_time, string E_time, string M_name, string Instructor, string H_id)
        {
            Labs lb = new Labs();
            lb.Session_id = Session_id;
            lb.B_code = B_code;
            lb.Group_no = Group_no;
            lb.date = date;
            lb.S_time = S_time;
            lb.E_time = E_time;
            lb.M_name = M_name;
            lb.Instructor = Instructor;
            lb.H_id = H_id;
            return lb.UpdateLab(lb.Session_id);
        }


        public DataTable ViewLab()
        {
            Labs Lb = new Labs();
            return Lb.ViewLab();
        }


        //Sports

        public string AddSport(string All_ID,string Allocation, string Club,string Date, string S_time, string E_time)
        {
            Sports spt = new Sports();
            spt.All_ID = All_ID;
            spt.Allocation = Allocation;
            spt.Club = Club;
            spt.Date = Date;
            spt.S_time = S_time;
            spt.E_time = E_time;
            return spt.AddSport();
        }


        public string DelSport(string All_ID)
        {
            Sports spt = new Sports();
            spt.All_ID = All_ID;
            return spt.DelSport(spt.All_ID);
        }

        public string UpdateSport(string All_ID, string Allocation, string Club, string Date, string S_time, string E_time)
        {
            Sports spt = new Sports();
            spt.All_ID = All_ID;
            spt.Allocation = Allocation;
            spt.Club = Club;
            spt.Date = Date;
            spt.S_time = S_time;
            spt.E_time = E_time;
            return spt.UpdateSport(spt.All_ID);
        }


        public DataTable ViewSport()
        {
            Sports spt = new Sports();
            return spt.ViewSport();
        }


        //Instructors
        public string AddInstructor(string Ins_Id, string I_Name, string Contact, string Email, string Faculty) {
            Instructors ins = new Instructors();
            ins.Ins_Id = Ins_Id;
            ins.I_Name = I_Name;
            ins.Contact = Contact;
            ins.Email = Email;
            ins.Faculty = Faculty;
            return ins.AddInstructor();
        }

       public DataTable ViewInstructors()
        {
            Instructors ins = new Instructors();
            return ins.ViewInstructors();
        }

        public string DelInstructor(string Ins_Id)
        {
            Instructors ins = new Instructors();
            ins.Ins_Id = Ins_Id;
            return ins.DelInstructor(ins.Ins_Id);
        }

        public string UpdateInstructor(string Ins_Id, string I_Name, string Contact, string Email, string Faculty)
        {
            Instructors ins = new Instructors();
            ins.Ins_Id = Ins_Id;
            ins.I_Name = I_Name;
            ins.Contact = Contact;
            ins.Email = Email;
            ins.Faculty = Faculty;
            return ins.UpdateInstructors(ins.Ins_Id);
        }


        // Lecturers

        public string AddLecturer(string Lecturer_Id, string L_Name, string Contact, string E_mail, string Faculty, string Qualification)
        {
            Lecturer lec = new Lecturer();
            lec.Lecturer_Id = Lecturer_Id;
            lec.L_Name = L_Name;
            lec.Contact = Contact;
            lec.Email = E_mail;
            lec.Faculty = Faculty;
            lec.Qualification = Qualification;
            return lec.AddLecturer();
        }

        public string DelLecturer(string Lecturer_Id)
        {
            Lecturer lec = new Lecturer();
            lec.Lecturer_Id = Lecturer_Id;
            return lec.DelLecturer(lec.Lecturer_Id);
        }

        public string UpdateLecturer(string Lecturer_Id, string L_Name, string Contact, string E_mail, string Faculty, string Qualification)
        {
            Lecturer lec = new Lecturer();
            lec.Lecturer_Id = Lecturer_Id;
            lec.L_Name = L_Name;
            lec.Contact = Contact;
            lec.Email = E_mail;
            lec.Faculty = Faculty;
            lec.Qualification = Qualification;
            return lec.UpdateLecturer(lec.Lecturer_Id);
        }


        public DataTable ViewLecturer()
        {
            Lecturer lec = new Lecturer();
            return lec.ViewLecturers();
        }



        //Batches
        public string AddBatch(string B_code, string Capacity, string B_no, string S_date, string E_date)
        {
            Batch ba = new Batch();
            ba.B_code = B_code;
            ba.Capacity = Capacity;
            ba.B_no = B_no;
            ba.S_date = S_date;
            ba.E_date = E_date;
            return ba.AddBatch();
        }


        public string DelBatch(string B_code)
        {
            Batch ba = new Batch();
            ba.B_code = B_code;
            return ba.DelBatch(ba.B_code);
        }

        public string UpdateBatch(string B_code, string Capacity, string B_no, string S_date, string E_date)
        {
            Batch ba = new Batch();
            ba.B_code = B_code;
            ba.Capacity = Capacity;
            ba.B_no = B_no;
            ba.S_date = S_date;
            ba.E_date = E_date;
            return ba.UpdateBatch(ba.B_code);
        }


        public DataTable ViewBatch()
        {
            Batch ba = new Batch();
            return ba.ViewBatch();
        }






        public DataTable ViewUserInfo(string table, string User_Name, string password)
        {
            custom cu = new custom();
            return cu.ViewUserInfo(table, User_Name, password);
        }


        public DataTable TimeTableInfo(string table, string B_code, string lecEmail)
        {
            custom cu = new custom();
            return cu.TimeTableInfo(table, B_code, lecEmail);
        }


        public DataTable Lab_Session_Time(string B_code, string lecName)
        {
            custom cu = new custom();
            return cu.Lab_Session_Time(B_code, lecName);
        }

        public DataTable SportTimeTable(string lecName)
        {
            custom cu = new custom();
            return cu.SportTimeTable(lecName);
        }








        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
    }
}
