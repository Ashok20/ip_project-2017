﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data;

namespace NSBMService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        string GetData(int value);

        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);

        //Lecture Halls
        [OperationContract]
        string AddLec(string lec_id, string B_code, string date, string S_time, string E_time, string M_name, string Lecturer, string H_id);

        [OperationContract]
        string DelLec(string lec_id);

        [OperationContract]
        string UpdateLec(string lec_id,string B_code, string date, string S_time, string E_time, string M_name, string Lecturer, string H_id);

        [OperationContract]
        DataTable ViewLec();

        //Lab Sessions

        [OperationContract]
        string AddLab(string Session_id,string B_code, string Group_no, string date,string S_time,string E_time,string M_name,string Instructor,string H_id);


        [OperationContract]
        DataTable ViewLab();


        [OperationContract]
        string DelLab(string Session_id);

        [OperationContract]
        string UpdateLab(string Session_id, string B_code, string Group_no, string date, string S_time, string E_time, string M_name, string Instructor, string H_id);



        //Sports
        [OperationContract]
        string AddSport(string All_ID, string Allocation, string Club,string date, string S_time, string E_time);

        [OperationContract]
        string DelSport(string All_ID);

        [OperationContract]
        string UpdateSport(string All_ID, string Allocation, string Club, string date, string S_time, string E_time);

        [OperationContract]
        DataTable ViewSport();


        //Instructors
        [OperationContract]
        string AddInstructor(string Ins_Id, string I_Name, string Contact, string Email, string Faculty);

        [OperationContract]
        DataTable ViewInstructors();

        [OperationContract]
        string DelInstructor(string Ins_Id);

        [OperationContract]
        string UpdateInstructor(string Ins_Id, string I_Name, string Contact, string Email, string Faculty);

        //Lecturer
        [OperationContract]
        string AddLecturer(string Lecturer_Id,string L_Name,string Contact,string E_mail,string Faculty,string Qualification);

        [OperationContract]
        DataTable ViewLecturer();

        [OperationContract]
        string DelLecturer(string Lecturer_Id);

        [OperationContract]
        string UpdateLecturer(string Lecturer_Id, string L_Name, string Contact, string E_mail, string Faculty, string Qualification);



        //Batches
        [OperationContract]
        string AddBatch(string B_code, string Capacity, string B_no, string S_date, string E_date);

        [OperationContract]
        DataTable ViewBatch();

        [OperationContract]
        string DelBatch(string B_code);

        [OperationContract]
        string UpdateBatch(string B_code, string Capacity, string B_no, string S_date, string E_date);





        //User Information
        [OperationContract]
        DataTable ViewUserInfo(string table, string User_Name, string password);

        //Timetable
        [OperationContract]
        DataTable TimeTableInfo(string table, string B_code, string lecEmail);


        //Lab TimeTable
        [OperationContract]
        DataTable Lab_Session_Time(string B_code, string lecName);


        //Sport TimeTable
        [OperationContract]
        DataTable SportTimeTable(string lecName);







        // TODO: Add your service operations here
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
}
