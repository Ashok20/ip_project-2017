﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace NSBMService
{
    public class Labs
    {
        public string Session_id;
        public string B_code;
        public string Group_no;
        public string date;
        public string S_time;
        public string E_time;
        public string M_name;
        public string Instructor;
        public string H_id;
        
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["NSBMdb"].ToString());

    public string AddLab()
        {
            SqlCommand cmd = new SqlCommand("insert into Lab_Session values (@Se_id,@B_code,@Group_no,@date,@S_time,@E_time,@M_name,@Instructor,@H_id)", con);

            cmd.Parameters.AddWithValue("@Se_id", Session_id);
            cmd.Parameters.AddWithValue("@B_code", B_code);
            cmd.Parameters.AddWithValue("@Group_no", Group_no);
            cmd.Parameters.AddWithValue("@date", date);
            cmd.Parameters.AddWithValue("@S_time", S_time);
            cmd.Parameters.AddWithValue("@E_time", E_time);
            cmd.Parameters.AddWithValue("@M_name", M_name);
            cmd.Parameters.AddWithValue("@Instructor", Instructor);
            cmd.Parameters.AddWithValue("@H_id", H_id);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
               
                return "Record added";
            }catch(SqlException ex)
            {
                return ex.ToString();
            }
            finally
            {
                con.Close();
            }

        }


        public string DelLab(string Session_id)
        {
            SqlCommand cmd = new SqlCommand("delete from Lab_Session where Session_id =" + Session_id, con);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return "Record deleted";
            }
            catch (SqlException ex)
            {
                return ex.ToString();
            }
        }


        public string UpdateLab(string Session_id)
        {
            SqlCommand cmd = new SqlCommand("update Lab_Session set Session_id=@Se_id,B_code= @B_code,Group_No = Group_no,Lab_Date = @date,S_time = @S_time,E_time = @E_time,M_name = @M_name,Instructor = @Instructor,H_id = @H_id where Session_id =" + Session_id, con);

            cmd.Parameters.AddWithValue("@Se_id", Session_id);
            cmd.Parameters.AddWithValue("@B_code", B_code);
            cmd.Parameters.AddWithValue("@Group_no", Group_no);
            cmd.Parameters.AddWithValue("@date", date);
            cmd.Parameters.AddWithValue("@S_time", S_time);
            cmd.Parameters.AddWithValue("@E_time", E_time);
            cmd.Parameters.AddWithValue("@M_name", M_name);
            cmd.Parameters.AddWithValue("@Instructor", Instructor);
            cmd.Parameters.AddWithValue("@H_id", H_id);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return "Record updated";
            }
            catch (SqlException ex)
            {
                return ex.ToString();
            }
        }


        public DataTable ViewLab()
        {
            SqlCommand cmd = new SqlCommand("select * from Lab_Session", con);
            DataTable dt = new DataTable();

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            adpt.Fill(dt);
            dt.TableName = "Lab_Session";
            return dt;
        }


    }
}