﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace NSBMService
{
    public class Lecturer
    {
        public string Lecturer_Id;
        public string L_Name;
        public string Contact;
        public string Email;
        public string Faculty;
        public string Qualification;

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["NSBMdb"].ToString());

        public string AddLecturer()
        {
            SqlCommand cmd = new SqlCommand("insert into Lecturer values(@Lecturer_Id,@L_Name,@Contact,@Email,@Faculty,@Qualification)", con);
            cmd.Parameters.AddWithValue("@Lecturer_Id", Lecturer_Id);
            cmd.Parameters.AddWithValue("@L_Name", L_Name);
            cmd.Parameters.AddWithValue("@Contact", Contact);
            cmd.Parameters.AddWithValue("@Email", Email);
            cmd.Parameters.AddWithValue("@Faculty", Faculty);
            cmd.Parameters.AddWithValue("@Qualification", Qualification);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return "Record added";
            }
            catch (SqlException ex)
            {
                return ex.ToString();
            }
        }

        public string DelLecturer(string Lecturer_Id)
        {
            SqlCommand cmd = new SqlCommand("delete from Lecturer where Lec_id =" + Lecturer_Id, con);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return "Record deleted";
            }
            catch (SqlException ex)
            {
                return ex.ToString();
            }
        }

        public string UpdateLecturer(string Lecturer_ID)
        {
            SqlCommand cmd = new SqlCommand("update Lecturer set Lec_id = @Lecturer_Id,L_Name = @L_Name,Contact = @Contact,Email = @Email,Faculty = @Faculty,Qalification = @Qualification where Lec_id =" + Lecturer_ID, con);
            cmd.Parameters.AddWithValue("@Lecturer_Id", Lecturer_Id);
            cmd.Parameters.AddWithValue("@L_Name", L_Name);
            cmd.Parameters.AddWithValue("@Contact", Contact);
            cmd.Parameters.AddWithValue("@Email", Email);
            cmd.Parameters.AddWithValue("@Faculty", Faculty);
            cmd.Parameters.AddWithValue("@Qualification", Qualification);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return "Record updated";
            }
            catch (SqlException ex)
            {
                return ex.ToString();
            }

        }


        public DataTable ViewLecturers()
        {
            SqlCommand cmd = new SqlCommand("select * from Lecturer", con);
            DataTable dt = new DataTable();

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            adpt.Fill(dt);
            dt.TableName = "Lecturer";
            return dt;
        }

    }
}