﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms;
using WindowsFormsApplication17.ServiceReference1;


namespace WindowsFormsApplication17
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            Form1 homeForm = new Form1();
            homeForm.Visible = true;

        }

        private void Form4_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'nSBMDataSet12.Clubs' table. You can move, or remove it, as needed.
            this.clubsTableAdapter.Fill(this.nSBMDataSet12.Clubs);
            // TODO: This line of code loads data into the 'nSBMDataSet11.Non_Acc_Halls' table. You can move, or remove it, as needed.
            this.non_Acc_HallsTableAdapter.Fill(this.nSBMDataSet11.Non_Acc_Halls);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Service1Client obj = new Service1Client();
            label7.Text = obj.AddSport(textBox1.Text, comboBox1.Text, comboBox2.Text, dateTimePicker1.Text, textBox4.Text, textBox5.Text);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Service1Client obj = new Service1Client();
            dataGridView1.DataSource = obj.ViewSport();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Service1Client obj = new Service1Client();
            label7.Text = obj.DelSport(textBox1.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Service1Client obj = new Service1Client();
            label7.Text = obj.UpdateSport(textBox1.Text, comboBox1.Text, comboBox2.Text, dateTimePicker1.Text, textBox4.Text, textBox5.Text);
        }

        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                textBox1.Text = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                comboBox1.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
                comboBox2.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
                dateTimePicker1.Text = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
                textBox4.Text = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();
                textBox5.Text = dataGridView1.SelectedRows[0].Cells[5].Value.ToString();
            }
        }
    }
}
