﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication17.ServiceReference1;

namespace WindowsFormsApplication17
{
    public partial class FormBatch : Form
    {
        public FormBatch()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            FormEdit editform = new FormEdit();
            editform.Visible = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Service1Client obj = new Service1Client();
            label6.Text = obj.AddBatch(textBox1.Text, textBox2.Text, textBox3.Text, dateTimePicker1.Text, dateTimePicker2.Text); 
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Service1Client obj = new Service1Client();
            dataGridView1.DataSource = obj.ViewBatch();
        }

        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                textBox1.Text = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                textBox2.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
                textBox3.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
                dateTimePicker1.Text = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
                dateTimePicker2.Text = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Service1Client obj = new Service1Client();
            label6.Text = obj.DelBatch(textBox1.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Service1Client obj = new Service1Client();
            label6.Text = obj.UpdateBatch(textBox1.Text, textBox2.Text, textBox3.Text, dateTimePicker1.Text, dateTimePicker2.Text);
        }
    }
}
