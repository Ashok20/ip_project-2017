﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication17.ServiceReference1;


namespace WindowsFormsApplication17
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            Form1 homeForm = new Form1();
            homeForm.Visible = true;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Service1Client obj = new Service1Client();
            label9.Text = obj.AddLec(textBox1.Text, comboBatch.Text, dateTimePicker1.Text, textBox4.Text, textBox7.Text, comboModule.Text, comboLecturer.Text, comboHall.Text);
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'nSBMDataSet5.Lecturer' table. You can move, or remove it, as needed.
            this.lecturerTableAdapter.Fill(this.nSBMDataSet5.Lecturer);
            // TODO: This line of code loads data into the 'nSBMDataSet4.Module' table. You can move, or remove it, as needed.
            this.moduleTableAdapter.Fill(this.nSBMDataSet4.Module);
            // TODO: This line of code loads data into the 'nSBMDataSet3.Acc_Hall' table. You can move, or remove it, as needed.
            this.acc_HallTableAdapter.Fill(this.nSBMDataSet3.Acc_Hall);
            // TODO: This line of code loads data into the 'nSBMDataSet2.Lec_module' table. You can move, or remove it, as needed.
            this.lec_moduleTableAdapter1.Fill(this.nSBMDataSet2.Lec_module);
            // TODO: This line of code loads data into the 'nSBMDataSet1.Lec_module' table. You can move, or remove it, as needed.
            this.lec_moduleTableAdapter.Fill(this.nSBMDataSet1.Lec_module);
            // TODO: This line of code loads data into the 'nSBMDataSet.Batch' table. You can move, or remove it, as needed.
            this.batchTableAdapter.Fill(this.nSBMDataSet.Batch);

        }

        private void fillByToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.lec_moduleTableAdapter.FillBy(this.nSBMDataSet1.Lec_module);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void fillByToolStripButton1_Click(object sender, EventArgs e)
        {
            try
            {
                this.lec_moduleTableAdapter1.FillBy(this.nSBMDataSet2.Lec_module);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void fillBy1ToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.lec_moduleTableAdapter.FillBy1(this.nSBMDataSet1.Lec_module);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void fillBy2ToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.lec_moduleTableAdapter.FillBy2(this.nSBMDataSet1.Lec_module);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Service1Client obj = new Service1Client();
            label9.Text = obj.DelLec(textBox1.Text);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Service1Client obj = new Service1Client();
            dataGridView1.DataSource = obj.ViewLec();

        }

        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {

            if (dataGridView1.SelectedRows.Count > 0)
            {
                textBox1.Text = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                comboBatch.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
                dateTimePicker1.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
                textBox4.Text = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
                textBox7.Text = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();
                comboModule.Text = dataGridView1.SelectedRows[0].Cells[5].Value.ToString();
                comboLecturer.Text = dataGridView1.SelectedRows[0].Cells[6].Value.ToString();
                comboHall.Text = dataGridView1.SelectedRows[0].Cells[7].Value.ToString();
            }

               
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Service1Client obj = new Service1Client();
            label9.Text = obj.UpdateLec(textBox1.Text, comboBatch.Text, dateTimePicker1.Text, textBox4.Text, textBox7.Text, comboModule.Text, comboLecturer.Text, comboHall.Text);

        }

        /*private void dataGridView1(object sender, DataGridViewCellEventArgs e)
        {

        }*/
    }
}
