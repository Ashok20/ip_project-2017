﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication17.ServiceReference1;

namespace WindowsFormsApplication17
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'nSBMDataSet10.Acc_Hall' table. You can move, or remove it, as needed.
            this.acc_HallTableAdapter.Fill(this.nSBMDataSet10.Acc_Hall);
            // TODO: This line of code loads data into the 'nSBMDataSet9.Batch' table. You can move, or remove it, as needed.
            this.batchTableAdapter.Fill(this.nSBMDataSet9.Batch);
            // TODO: This line of code loads data into the 'nSBMDataSet8.Non_Acc_Halls' table. You can move, or remove it, as needed.
            this.non_Acc_HallsTableAdapter.Fill(this.nSBMDataSet8.Non_Acc_Halls);
            // TODO: This line of code loads data into the 'nSBMDataSet7.Instructor' table. You can move, or remove it, as needed.
            this.instructorTableAdapter.Fill(this.nSBMDataSet7.Instructor);
            // TODO: This line of code loads data into the 'nSBMDataSet6.Module' table. You can move, or remove it, as needed.
            this.moduleTableAdapter.Fill(this.nSBMDataSet6.Module);

        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            Form1 homeForm = new Form1();
            homeForm.Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Service1Client obj = new Service1Client();
            label11.Text =obj.UpdateLab(textBox1.Text, comboBatch.Text, textBox2.Text, dateTimePicker1.Text, textBox4.Text, textBox7.Text, comboModule.Text, comboInstructor.Text, comboHall.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Service1Client obj = new Service1Client();
            label11.Text = obj.AddLab(textBox1.Text, comboBatch.Text, textBox2.Text, dateTimePicker1.Text, textBox4.Text, textBox7.Text, comboModule.Text, comboInstructor.Text, comboHall.Text);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Service1Client obj = new Service1Client();
            dataGridView1.DataSource = obj.ViewLab();
        }

        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                textBox1.Text = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                comboBatch.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
                textBox2.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
                dateTimePicker1.Text = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
                textBox4.Text = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();
                textBox7.Text = dataGridView1.SelectedRows[0].Cells[5].Value.ToString();
                comboModule.Text = dataGridView1.SelectedRows[0].Cells[6].Value.ToString();
                comboInstructor.Text = dataGridView1.SelectedRows[0].Cells[7].Value.ToString();
                comboHall.Text = dataGridView1.SelectedRows[0].Cells[8].Value.ToString();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Service1Client obj = new Service1Client();
            label11.Text = obj.DelLab(textBox1.Text);
        }

        /*private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }*/
    }
}
