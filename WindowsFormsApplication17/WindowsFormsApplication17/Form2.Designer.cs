﻿namespace WindowsFormsApplication17
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.comboBatch = new System.Windows.Forms.ComboBox();
            this.batchBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nSBMDataSet = new WindowsFormsApplication17.NSBMDataSet();
            this.batchTableAdapter = new WindowsFormsApplication17.NSBMDataSetTableAdapters.BatchTableAdapter();
            this.comboModule = new System.Windows.Forms.ComboBox();
            this.moduleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nSBMDataSet4 = new WindowsFormsApplication17.NSBMDataSet4();
            this.lecmoduleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nSBMDataSet1 = new WindowsFormsApplication17.NSBMDataSet1();
            this.lec_moduleTableAdapter = new WindowsFormsApplication17.NSBMDataSet1TableAdapters.Lec_moduleTableAdapter();
            this.comboLecturer = new System.Windows.Forms.ComboBox();
            this.lecturerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nSBMDataSet5 = new WindowsFormsApplication17.NSBMDataSet5();
            this.lecmoduleBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.nSBMDataSet2 = new WindowsFormsApplication17.NSBMDataSet2();
            this.lec_moduleTableAdapter1 = new WindowsFormsApplication17.NSBMDataSet2TableAdapters.Lec_moduleTableAdapter();
            this.comboHall = new System.Windows.Forms.ComboBox();
            this.accHallBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nSBMDataSet3 = new WindowsFormsApplication17.NSBMDataSet3();
            this.acc_HallTableAdapter = new WindowsFormsApplication17.NSBMDataSet3TableAdapters.Acc_HallTableAdapter();
            this.moduleTableAdapter = new WindowsFormsApplication17.NSBMDataSet4TableAdapters.ModuleTableAdapter();
            this.lecturerTableAdapter = new WindowsFormsApplication17.NSBMDataSet5TableAdapters.LecturerTableAdapter();
            this.label9 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.studentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.batchBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nSBMDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moduleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nSBMDataSet4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lecmoduleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nSBMDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lecturerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nSBMDataSet5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lecmoduleBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nSBMDataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.accHallBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nSBMDataSet3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.studentBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Agency FB", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(540, 14);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(199, 39);
            this.label1.TabIndex = 0;
            this.label1.Text = "HALL ALLOCATION";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Impact", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(35, 99);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 29);
            this.label2.TabIndex = 1;
            this.label2.Text = "LEC NO";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Impact", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(35, 151);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 29);
            this.label3.TabIndex = 2;
            this.label3.Text = "BATCH";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Impact", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(35, 211);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 29);
            this.label4.TabIndex = 3;
            this.label4.Text = "DATE";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Impact", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(35, 269);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 29);
            this.label5.TabIndex = 4;
            this.label5.Text = "START TIME";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Impact", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(35, 382);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 29);
            this.label6.TabIndex = 5;
            this.label6.Text = "MODULE";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Impact", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label7.Location = new System.Drawing.Point(35, 451);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 29);
            this.label7.TabIndex = 6;
            this.label7.Text = "LECTURE";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(201, 99);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(190, 26);
            this.textBox1.TabIndex = 7;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(201, 269);
            this.textBox4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(190, 26);
            this.textBox4.TabIndex = 10;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(40, 629);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(127, 74);
            this.button1.TabIndex = 13;
            this.button1.Text = "ADD";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(215, 629);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(127, 74);
            this.button2.TabIndex = 14;
            this.button2.Text = "UPDATE";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(351, 629);
            this.button3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(127, 74);
            this.button3.TabIndex = 15;
            this.button3.Text = "DELETE";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(511, 99);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1132, 371);
            this.dataGridView1.TabIndex = 17;
            this.dataGridView1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dataGridView1_MouseClick);
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(202, 318);
            this.textBox7.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(188, 26);
            this.textBox7.TabIndex = 19;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Impact", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label8.Location = new System.Drawing.Point(35, 318);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 29);
            this.label8.TabIndex = 18;
            this.label8.Text = "END TIME";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Impact", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label10.Location = new System.Drawing.Point(35, 529);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 29);
            this.label10.TabIndex = 22;
            this.label10.Text = "HALL ID";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "yyyy-MM-dd";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(201, 211);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(190, 26);
            this.dateTimePicker1.TabIndex = 24;
            // 
            // comboBatch
            // 
            this.comboBatch.DataSource = this.batchBindingSource;
            this.comboBatch.DisplayMember = "B_code";
            this.comboBatch.FormattingEnabled = true;
            this.comboBatch.Location = new System.Drawing.Point(201, 149);
            this.comboBatch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.comboBatch.Name = "comboBatch";
            this.comboBatch.Size = new System.Drawing.Size(190, 28);
            this.comboBatch.TabIndex = 25;
            this.comboBatch.ValueMember = "B_code";
            // 
            // batchBindingSource
            // 
            this.batchBindingSource.DataMember = "Batch";
            this.batchBindingSource.DataSource = this.nSBMDataSet;
            // 
            // nSBMDataSet
            // 
            this.nSBMDataSet.DataSetName = "NSBMDataSet";
            this.nSBMDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // batchTableAdapter
            // 
            this.batchTableAdapter.ClearBeforeFill = true;
            // 
            // comboModule
            // 
            this.comboModule.DataSource = this.moduleBindingSource;
            this.comboModule.DisplayMember = "M_name";
            this.comboModule.FormattingEnabled = true;
            this.comboModule.Location = new System.Drawing.Point(201, 386);
            this.comboModule.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.comboModule.Name = "comboModule";
            this.comboModule.Size = new System.Drawing.Size(190, 28);
            this.comboModule.TabIndex = 26;
            this.comboModule.ValueMember = "M_name";
            // 
            // moduleBindingSource
            // 
            this.moduleBindingSource.DataMember = "Module";
            this.moduleBindingSource.DataSource = this.nSBMDataSet4;
            // 
            // nSBMDataSet4
            // 
            this.nSBMDataSet4.DataSetName = "NSBMDataSet4";
            this.nSBMDataSet4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lecmoduleBindingSource
            // 
            this.lecmoduleBindingSource.DataMember = "Lec_module";
            this.lecmoduleBindingSource.DataSource = this.nSBMDataSet1;
            // 
            // nSBMDataSet1
            // 
            this.nSBMDataSet1.DataSetName = "NSBMDataSet1";
            this.nSBMDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lec_moduleTableAdapter
            // 
            this.lec_moduleTableAdapter.ClearBeforeFill = true;
            // 
            // comboLecturer
            // 
            this.comboLecturer.DataSource = this.lecturerBindingSource;
            this.comboLecturer.DisplayMember = "L_Name";
            this.comboLecturer.FormattingEnabled = true;
            this.comboLecturer.Location = new System.Drawing.Point(202, 460);
            this.comboLecturer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.comboLecturer.Name = "comboLecturer";
            this.comboLecturer.Size = new System.Drawing.Size(188, 28);
            this.comboLecturer.TabIndex = 27;
            this.comboLecturer.ValueMember = "L_Name";
            // 
            // lecturerBindingSource
            // 
            this.lecturerBindingSource.DataMember = "Lecturer";
            this.lecturerBindingSource.DataSource = this.nSBMDataSet5;
            // 
            // nSBMDataSet5
            // 
            this.nSBMDataSet5.DataSetName = "NSBMDataSet5";
            this.nSBMDataSet5.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lecmoduleBindingSource1
            // 
            this.lecmoduleBindingSource1.DataMember = "Lec_module";
            this.lecmoduleBindingSource1.DataSource = this.nSBMDataSet2;
            // 
            // nSBMDataSet2
            // 
            this.nSBMDataSet2.DataSetName = "NSBMDataSet2";
            this.nSBMDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lec_moduleTableAdapter1
            // 
            this.lec_moduleTableAdapter1.ClearBeforeFill = true;
            // 
            // comboHall
            // 
            this.comboHall.DataSource = this.accHallBindingSource;
            this.comboHall.DisplayMember = "H_id";
            this.comboHall.FormattingEnabled = true;
            this.comboHall.Location = new System.Drawing.Point(199, 524);
            this.comboHall.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.comboHall.Name = "comboHall";
            this.comboHall.Size = new System.Drawing.Size(192, 28);
            this.comboHall.TabIndex = 28;
            this.comboHall.ValueMember = "H_id";
            // 
            // accHallBindingSource
            // 
            this.accHallBindingSource.DataMember = "Acc_Hall";
            this.accHallBindingSource.DataSource = this.nSBMDataSet3;
            // 
            // nSBMDataSet3
            // 
            this.nSBMDataSet3.DataSetName = "NSBMDataSet3";
            this.nSBMDataSet3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // acc_HallTableAdapter
            // 
            this.acc_HallTableAdapter.ClearBeforeFill = true;
            // 
            // moduleTableAdapter
            // 
            this.moduleTableAdapter.ClearBeforeFill = true;
            // 
            // lecturerTableAdapter
            // 
            this.lecturerTableAdapter.ClearBeforeFill = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(934, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 20);
            this.label9.TabIndex = 29;
            this.label9.Text = "label9";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(1505, 480);
            this.button5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(137, 74);
            this.button5.TabIndex = 30;
            this.button5.Text = "View";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // studentBindingSource
            // 
            this.studentBindingSource.DataMember = "Student";
            this.studentBindingSource.DataSource = this.nSBMDataSet13;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(63)))), ((int)(((byte)(90)))));
            this.ClientSize = new System.Drawing.Size(1657, 765);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.comboHall);
            this.Controls.Add(this.comboLecturer);
            this.Controls.Add(this.comboModule);
            this.Controls.Add(this.comboBatch);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.batchBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nSBMDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moduleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nSBMDataSet4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lecmoduleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nSBMDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lecturerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nSBMDataSet5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lecmoduleBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nSBMDataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.accHallBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nSBMDataSet3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.studentBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.ComboBox comboBatch;
        private NSBMDataSet nSBMDataSet;
        private System.Windows.Forms.BindingSource batchBindingSource;
        private NSBMDataSetTableAdapters.BatchTableAdapter batchTableAdapter;
        private System.Windows.Forms.ComboBox comboModule;
        private NSBMDataSet1 nSBMDataSet1;
        private System.Windows.Forms.BindingSource lecmoduleBindingSource;
        private NSBMDataSet1TableAdapters.Lec_moduleTableAdapter lec_moduleTableAdapter;
        private System.Windows.Forms.ComboBox comboLecturer;
        private NSBMDataSet2 nSBMDataSet2;
        private System.Windows.Forms.BindingSource lecmoduleBindingSource1;
        private NSBMDataSet2TableAdapters.Lec_moduleTableAdapter lec_moduleTableAdapter1;
        private System.Windows.Forms.ComboBox comboHall;
        private NSBMDataSet3 nSBMDataSet3;
        private System.Windows.Forms.BindingSource accHallBindingSource;
        private NSBMDataSet3TableAdapters.Acc_HallTableAdapter acc_HallTableAdapter;
        private NSBMDataSet4 nSBMDataSet4;
        private System.Windows.Forms.BindingSource moduleBindingSource;
        private NSBMDataSet4TableAdapters.ModuleTableAdapter moduleTableAdapter;
        private NSBMDataSet5 nSBMDataSet5;
        private System.Windows.Forms.BindingSource lecturerBindingSource;
        private NSBMDataSet5TableAdapters.LecturerTableAdapter lecturerTableAdapter;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button5;
        private NSBMDataSet13 nSBMDataSet13;
        private System.Windows.Forms.BindingSource studentBindingSource;
        private NSBMDataSet13TableAdapters.StudentTableAdapter studentTableAdapter;
    }
}