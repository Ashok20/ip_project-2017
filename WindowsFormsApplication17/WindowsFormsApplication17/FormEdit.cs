﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication17
{
    public partial class FormEdit : Form
    {
        public FormEdit()
        {
            InitializeComponent();
            button3.Visible = false;
            button5.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            FormLecture editLec = new FormLecture();
            editLec.Visible = true;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            FormInstructors editinstuctor = new FormInstructors();
            editinstuctor.Visible = true;

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            FormModule editmodule = new FormModule();
            editmodule.Visible = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            FormBatch editbatch = new FormBatch();
            editbatch.Visible = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            FormClubcs editclubs = new FormClubcs();
            editclubs.Visible = true;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            Form1 homeform = new Form1();
            homeform.Visible = true;
        }

        private void FormEdit_Load(object sender, EventArgs e)
        {

        }
    }
}
