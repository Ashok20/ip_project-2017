﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
//using System.Data;

namespace WindowsFormsApplication17
{
    public partial class Form1 : Form
    {
        public Form1()
        {
                InitializeComponent();
                button1.Enabled = false;
                button2.Enabled = false;
                button3.Enabled = false;
                button7.Enabled = false;
            
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if(slideBar.Height==447 & slideBar.Width == 165)
            {
                slideBar.Height = 447;
                slideBar.Width = 74;
                //reSlide.Visible = false; 
            }
            else
            {
                slideBar.Height = 447;
                slideBar.Width = 165;
                //reSlide.Visible = true; 

            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection con = new SqlConnection("Data Source=.;Initial Catalog=NSBM;Integrated Security=True");

                SqlCommand cmd = new SqlCommand("select * from Admin where Username = @Username and Password = @Password", con);
                con.Open();
                cmd.Parameters.AddWithValue("@Username", Uname.Text);
                cmd.Parameters.AddWithValue("@Password", Pwd.Text);
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows == true)
                {
                    MessageBox.Show("Login Success");
                    panel3.Hide();
                    pictureBox2.Show();
                    button6.Show();
                    AdminIcon.Show();
                    label5.Text = Uname.Text;
                    label5.Show();
                    button1.Enabled = true;
                    button2.Enabled = true;
                    button3.Enabled = true;
                    button7.Enabled = true;
                }
                else
                {
                    MessageBox.Show("Incorrect username or Password");
                }
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }

            Uname.Clear();
            Pwd.Clear();


        }

        

        /*private void Form1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }*/

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

            //pictureBox2.Hide();
            button6.Hide();
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //this.Visible = false;
            Form2 hallForm = new Form2();
            hallForm.Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //this.Visible = false;
            Form3 labForm = new Form3();
            labForm.Visible = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //this.Visible = false;
            Form4 nonForm = new Form4();
            nonForm.Visible = true;
        }

        private void reSlide_Click(object sender, EventArgs e)
        {
            if (slideBar.Height == 447 & slideBar.Width == 196)
            {
                slideBar.Height = 447;
                slideBar.Width = 46;
                //reSlide.Visible = false; 
            }
            else
            {
                slideBar.Height = 447;
                slideBar.Width = 196;
                //reSlide.Visible = true; 

            }
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = false;
            button7.Enabled = false;
            panel3.Show();
            AdminIcon.Hide();
            label5.Hide();
            button6.Hide();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            //this.Visible = false;
            FormEdit EditForm = new FormEdit();
            EditForm.Visible = true;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void slideBar_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            AdminIcon.Hide();
            label5.Hide();
            
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            panel3.Hide();
            pictureBox2.Show();
            button6.Show();
            AdminIcon.Show();
            label5.Text = "USER";
            label5.Show();
        }


        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
    }
}
